import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");


describe('Bonus system tests', () => {
    let app;
    const standard_coefficient = 0.05;
    const premium_coefficient = 0.1;
    const diamond_coefficient = 0.2;
    const other_coefficient = 0;
    const small_bonus = 1;
    const normal_bonus = 1.5;
    const big_bonus = 2;
    const huge_bonus = 2.5
    const small_bonus_amount = 1337;
    const normal_bonus_bound_amount = 10000;
    const normal_bonus_amount = 13370;
    const big_bonus_bound_amount = 50000;
    const big_bonus_amount = 63370;
    const huge_bonus_bound_amount = 100000;
    const huge_bonus_amount = 133700;




    console.log("Tests started");

    test('Valid Standard small bonus test', (done) => {
        let a = "Standard";
        let b = small_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(small_bonus * standard_coefficient);
        done();
    });

    test('Valid Standard normal bonus test', (done) => {
        let a = "Standard";
        let b = normal_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(normal_bonus * standard_coefficient);
        done();
    });

    test('Valid Standard normal bonus bound test', (done) => {
        let a = "Standard";
        let b = normal_bonus_bound_amount;
        expect(calculateBonuses(a, b)).toEqual(normal_bonus * standard_coefficient);
        done();
    });

    test('Valid Standard big bonus test', (done) => {
        let a = "Standard";
        let b = big_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(big_bonus * standard_coefficient);
        done();
    });

    test('Valid Standard big bonus bound test', (done) => {
        let a = "Standard";
        let b = big_bonus_bound_amount;
        expect(calculateBonuses(a, b)).toEqual(big_bonus * standard_coefficient);
        done();
    });

    test('Valid Standard huge bonus test', (done) => {
        let a = "Standard";
        let b = huge_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(huge_bonus * standard_coefficient);
        done();
    });

    test('Valid Standard huge bonus bound test', (done) => {
        let a = "Standard";
        let b = huge_bonus_bound_amount;
        expect(calculateBonuses(a, b)).toEqual(huge_bonus * standard_coefficient);
        done();
    });

    test('Valid Premium small bonus test', (done) => {
        let a = "Premium";
        let b = small_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(small_bonus * premium_coefficient);
        done();
    });

    test('Valid Premium normal bonus test', (done) => {
        let a = "Premium";
        let b = normal_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(normal_bonus * premium_coefficient);
        done();
    });

    test('Valid Premium big bonus test', (done) => {
        let a = "Premium";
        let b = big_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(big_bonus * premium_coefficient);
        done();
    });

    test('Valid Premium huge bonus test', (done) => {
        let a = "Premium";
        let b = huge_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(huge_bonus * premium_coefficient);
        done();
    });

    test('Valid Diamond small bonus test', (done) => {
        let a = "Diamond";
        let b = small_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(small_bonus * diamond_coefficient);
        done();
    });

    test('Valid Diamond normal bonus test', (done) => {
        let a = "Diamond";
        let b = normal_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(normal_bonus * diamond_coefficient);
        done();
    });

    test('Valid Diamond big bonus test', (done) => {
        let a = "Diamond";
        let b = big_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(big_bonus * diamond_coefficient);
        done();
    });

    test('Valid Diamond huge bonus test', (done) => {
        let a = "Diamond";
        let b = huge_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(huge_bonus * diamond_coefficient);
        done();
    });

    test('Valid Other small bonus test', (done) => {
        let a = "Aboba";
        let b = small_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(small_bonus * other_coefficient);
        done();
    });

    test('Valid Other normal bonus test', (done) => {
        let a = "Aboba";
        let b = normal_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(normal_bonus * other_coefficient);
        done();
    });

    test('Valid Other big bonus test', (done) => {
        let a = "Aboba";
        let b = big_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(big_bonus * other_coefficient);
        done();
    });

    test('Valid Other huge bonus test', (done) => {
        let a = "Aboba";
        let b = huge_bonus_amount;
        expect(calculateBonuses(a, b)).toEqual(huge_bonus * other_coefficient);
        done();
    });

});
